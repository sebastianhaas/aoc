expense_report = [int(line.rstrip('\n')) for line in open("input", "r")]

for entryA in expense_report:
	for entryB in expense_report:
		if entryA != entryB:
			if (entryA + entryB) == 2020:
				print(f"Result: {entryA * entryB}")
				exit()
