expense_report = [int(line.rstrip('\n')) for line in open("input", "r")]

for entryA in expense_report:
	for entryB in expense_report:
		if entryA != entryB:
			for entryC in expense_report:
				if entryC != entryA and entryC != entryB:
					if (entryA + entryB + entryC) == 2020:
						print(f"Result: {entryA * entryB * entryC}")
						exit()
