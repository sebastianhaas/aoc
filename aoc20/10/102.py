arr = [int(line.rstrip()) for line in open('input_test', 'r').readlines()]
arr.sort()
arr.append(arr[-1]+3)
print(arr)

memo = {0: 1}
print(memo)
for r in arr:
  memo[r] = memo.get(r-3,0) + memo.get(r-2,0) + memo.get(r-1,0)
  print(memo)
print(memo[arr[-1]])
