{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Main where

import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import Data.List (sort, sortOn)
import Data.Function (on)
import Data.Graph.Inductive.Graph (mkGraph, LEdge, UNode, Node, order, suc, lab, lsuc, Graph)
import Data.Graph.Inductive.Query (bfs, level, bfe)
import Data.Graph.Inductive.PatriciaTree (Gr)

get_neighbours_sorted :: (Graph gr) => gr () Int -> Node -> [(Node, Int)]
get_neighbours_sorted graph node = sortOn (\(node, elabel) -> elabel) (lsuc graph node)

get_compatible_adapters :: [Int] -> Int -> [LEdge Int]
get_compatible_adapters adapters x = map (\neighbour -> (x, neighbour, neighbour-x)) (filter (\adap -> adap > x && adap <= x+3) adapters)

find_next_node :: (Graph gr) => gr () Int -> Node -> (Node, Int)
find_next_node graph node = head (get_neighbours_sorted graph node)

has_next_node :: (Graph gr) => gr () Int -> Node -> Bool
has_next_node graph node =
    if null (get_neighbours_sorted graph node)
    then False
    else True

find_chain_r :: (Graph gr) => gr () Int -> [(Node, Int)] -> [(Node, Int)]
find_chain_r graph path =
    if has_next_node graph (fst (last path))
    then find_chain_r graph (find_chain_r graph (path ++ [find_next_node graph (fst (last path))] ))
    else path

find_chain :: (Graph gr) => gr () Int -> [(Node, Int)]
find_chain graph = find_chain_r graph [(1, 1)]

count_diff :: (Node, Int) -> (Int, Int) -> (Int, Int)
count_diff step count =
    if (snd step) == 1
    then ((fst count) + 1, (snd count) + 0)
    else ((fst count) + 0, (snd count) + 1)

get_jolt_differences :: [(Node, Int)] -> (Int, Int)
get_jolt_differences path = foldl (\acc x -> (count_diff x acc)) (0, 0) path

main :: IO ()
main = do
    ls <- fmap Text.lines (Text.readFile "../input_test")
    
    let strings = map Text.unpack ls
    let adapters = sort (map read strings :: [Int])

    let nodes :: [UNode] = map (\x -> (x, ())) adapters
    let edges :: [LEdge Int] = concat (map (\x -> (get_compatible_adapters adapters x)) adapters)

    let d :: Gr () Int = mkGraph nodes edges
    let chain = find_chain d
    let diff = get_jolt_differences chain
    print "1-differences:"
    print (fst diff)
    print "3-differences:"
    print ((snd diff) + 1)
    print "Multiplied:"
    print ((fst diff) * ((snd diff) + 1))
