function readinput(filename)
    B = Array{Array{Char}}(undef, 0)

    open(filename,"r") do file
        line = Array{Char}(undef, 0)
        while !eof(file)
            c = read(file, Char)
            if (c != '\n')
                push!(line, c)
            else
                push!(B, line)
                line = Array{Char}(undef, 0)
            end
        end
    end
    
    return [B[i][j] for i in 1:size(B, 1), j in 1:size(B[1], 1)]
end

function count_occupied_seats(plan)
    return count(i-> i == '#', plan)
end

function get_adjacent_seats(B, u, v)
    yMax = size(B, 1)
    xMax = size(B, 2)
    ajdacent_seats = Array{Char}(undef, 0)
    for y in v-1:v+1
        if y > 0 && y <= yMax
            for x in u-1:u+1
                if x > 0 && x <= xMax
                    if x != u || y != v
                        push!(ajdacent_seats, B[y, x])
                    end
                end
            end
        end
    end
    return ajdacent_seats
end

function handle_seat(A, x, y)
    seat = A[y, x]
    if seat == 'L'
        adj = get_adjacent_seats(A, x, y)
        if !any(i -> i == '#', adj)
            return '#'
        end
    elseif seat == '#'
        adj = get_adjacent_seats(A, x, y)
        if count(i-> i == '#', adj) >= 4
            return 'L'
        end
    end
    return seat
end

function exec_round(plan)
    B = deepcopy(plan)
    for y in 1:size(plan, 1)
        for x in 1:(size(plan, 2))
            B[y, x] = handle_seat(plan, x, y)
        end
    end
    return B
end

function print_seat_plan(plan)
    for y in 1:size(plan, 1)
        for x in 1:(size(plan, 2))
            print(plan[y,x])
        end
        println()
    end
end

function play(plan)
    X = undef
    Y = deepcopy(plan)
    while (X != Y)
        X = deepcopy(Y)
        Y = exec_round(X)
    end
    return Y
end

input = readinput("input")
println("Input:")
print_seat_plan(input)
println()

X = play(input)
println("Final:")
print_seat_plan(X)
println()

println("$(count_occupied_seats(X)) seats end up occupied.")
