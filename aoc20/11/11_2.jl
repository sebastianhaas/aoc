# import Pkg; Pkg.add("Combinatorics")
using Combinatorics

function readinput(filename)
    B = Array{Array{Char}}(undef, 0)

    open(filename,"r") do file
        line = Array{Char}(undef, 0)
        while !eof(file)
            c = read(file, Char)
            if (c != '\n')
                push!(line, c)
            else
                push!(B, line)
                line = Array{Char}(undef, 0)
            end
        end
    end
    
    return [B[i][j] for i in 1:size(B, 1), j in 1:size(B[1], 1)]
end

function count_occupied_seats(plan)
    return count(i-> i == '#', plan)
end

function get_seats_in_sight(B, u, v)
    yMax = size(B, 2)
    xMax = size(B, 1)
    dirs = Iterators.product(-1:1, -1:1) |> collect |> dirs -> filter(x -> x != (0,0), dirs)
    seats_in_sight = Array{Union{Char, Nothing}}(undef, 0)
    foreach(dir -> push!(seats_in_sight, get_first_seat_in_direction(B, u, v, dir[1], dir[2])), dirs)
    return seats_in_sight
end

function get_first_seat_in_direction(B, u, v, x, y)
    yMax = size(B, 1)
    xMax = size(B, 2)
    while true
        u = u + x
        v = v + y
        if v > 0 && v <= yMax && u > 0 && u <= xMax
            seat = B[v, u]
            if seat == '#' || seat == 'L'
                return seat
            end
        else
            return nothing
        end
    end
end

function handle_seat(A, x, y)
    seat = A[y, x]
    if seat == 'L'
        adj = get_seats_in_sight(A, x, y)
        if !any(i -> i == '#', adj)
            return '#'
        end
    elseif seat == '#'
        adj = get_seats_in_sight(A, x, y)
        if count(i-> i == '#', adj) >= 5
            return 'L'
        end
    end
    return seat
end

function exec_round(plan)
    B = deepcopy(plan)
    for y in 1:size(plan, 1)
        for x in 1:(size(plan, 2))
            B[y, x] = handle_seat(plan, x, y)
        end
    end
    return B
end

function print_seat_plan(plan)
    for y in 1:size(plan, 1)
        for x in 1:(size(plan, 2))
            print(plan[y,x])
        end
        println()
    end
end

function play(plan)
    X = undef
    Y = deepcopy(plan)
    while (X != Y)
        X = deepcopy(Y)
        Y = exec_round(X)
    end
    return Y
end

input = readinput("input")
println("Input:")
print_seat_plan(input)
println()

X = play(input)
println("Final:")
print_seat_plan(X)
println()

println("$(count_occupied_seats(X)) seats end up occupied.")
