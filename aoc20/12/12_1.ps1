$dir = 90
$x = 0
$y = 0

foreach($line in Get-Content input) {
    $cmd = $line.Chars(0)
    $amount = $line.Substring(1) -as [int]

    if ($cmd -eq 'F') {
        if ($dir -eq 0) {
            $y = $y - $amount
        } elseif ($dir -eq 90) {
            $x = $x + $amount
        } elseif ($dir -eq 180) {
            $y = $y + $amount
        } elseif ($dir -eq 270) {
            $x = $x - $amount
        } else {
            Write-Output $dir
            throw "Not good."
        }
    } elseif ($cmd -eq 'N') {
        $y = $y - $amount
    } elseif ($cmd -eq 'E') {
        $x = $x + $amount
    } elseif ($cmd -eq 'S') {
        $y = $y + $amount
    } elseif ($cmd -eq 'W') {
        $x = $x - $amount
    } elseif ($cmd -eq 'L') {
        $dir = ($dir - $amount) % 360
        if ($dir -eq -90) {
            $dir = 270
        } elseif ($dir -eq -180) {
            $dir = 180
        } elseif ($dir -eq -270) {
            $dir = 90
        }
    } elseif ($cmd -eq 'R') {
        $dir = ($dir + $amount) % 360
        if ($dir -eq -90) {
            $dir = 270
        } elseif ($dir -eq -180) {
            $dir = 180
        } elseif ($dir -eq -270) {
            $dir = 90
        }
    } else {
        throw "Not good."
    }
}

Write-Output "The final position is:" $x $y
Write-Output "Manhattan-Distance from the start:" ([math]::Abs($shipx) + [math]::Abs($shipy))
