$dir = 90
$x = 10
$y = -1

$shipx = 0
$shipy = 0

foreach($line in Get-Content input) {
    $cmd = $line.Chars(0)
    $amount = $line.Substring(1) -as [int]

    if ($cmd -eq 'F') {
        $shipx = $shipx + ($amount * $x)
        $shipy = $shipy + ($amount * $y)
    } elseif ($cmd -eq 'N') {
        $y = $y - $amount
    } elseif ($cmd -eq 'E') {
        $x = $x + $amount
    } elseif ($cmd -eq 'S') {
        $y = $y + $amount
    } elseif ($cmd -eq 'W') {
        $x = $x - $amount
    } elseif ($cmd -eq 'L') {
        if ($amount -eq 90) {
            $xtmp = $x
            $ytmp = $y
            $x = $ytmp
            $y = $xtmp * -1
        } elseif ($amount -eq 180) {
            $xtmp = $x
            $ytmp = $y
            $x = $xtmp * -1
            $y = $ytmp * -1
        } elseif ($amount -eq 270) {
            $xtmp = $x
            $ytmp = $y
            $x = $ytmp * -1
            $y = $xtmp
        }
    } elseif ($cmd -eq 'R') {
        if ($amount -eq 90) {
            $xtmp = $x
            $ytmp = $y
            $x = $ytmp * -1
            $y = $xtmp
        } elseif ($amount -eq 180) {
            $xtmp = $x
            $ytmp = $y
            $x = $xtmp * -1
            $y = $ytmp * -1
        } elseif ($amount -eq 270) {
            $xtmp = $x
            $ytmp = $y
            $x = $ytmp
            $y = $xtmp * -1
        }
    } else {
        throw "Not good."
    }
    Write-Output $x$y
    Write-Output $shipx$shipy
}

Write-Output "The final position is:" $shipx $shipy
Write-Output "Manhattan-Distance from the start:" ([math]::Abs($shipx) + [math]::Abs($shipy))
