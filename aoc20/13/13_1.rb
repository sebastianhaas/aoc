def get_next_departure(timestamp, busline)
    ndep = timestamp
    while (ndep % busline != 0)
        ndep = ndep + 1
    end
    return ndep
end

a = IO.readlines("input_test")
a[1] = a[1].strip!.split(',')
print a[0]
print a[1]

timestamp = a[0].to_i
buses = a[1].select { |n| n != 'x' }.map { |n| n.to_i }
print buses

deps = buses.map { |bus| [get_next_departure(timestamp, bus), bus] }
next_dep = deps.sort

print(next_dep)

print((next_dep[0][0] - timestamp) * next_dep[0])
