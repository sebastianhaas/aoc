# a = IO.readlines("input")
# a[1] = a[1].strip!.split(',')

# buses = a[1].map { |n| n.to_i }
# print buses

# buses.each_with_index do |object, index|
#     puts "#{object} at index #{index}"
# end

def get_smallest_solution(buses_with_offsets, start, step_size)
    t = start
    while (true)
        if buses_with_offsets.all? { |bo| condition(bo[0], bo[1], t) }
            return t
        else
            t = t + step_size
        end
    end
end

def condition(a, offset, t)
    return (t + offset) % a == 0
end

# This was reduced from the output of line 8. Could be done in code as well.
inputs = [[41, 0], [37, 35], [431, 41], [23, 49], [13, 54], [17, 58], [19, 60], [863, 72], [29, 101]]

start = inputs[0][0]
step_size = start
lines_wo = []
inputs.each do |bus_with_offset|
    lines_wo.push(bus_with_offset)
    start = get_smallest_solution(lines_wo, start, step_size)
    step_size = lines_wo.reduce(1) { |acc, bo| acc * bo[0] }
    #puts start
    #puts step_size
end

puts start
