def mask
def memory = []

def toBinaryUInt36(value) {
    return value.toString(2).padLeft(36, "0")
}

def fromBinaryUInt36(binaryUInt36) {
    return new BigInteger(binaryUInt36, 2)
}

def applyMask(mask, binaryUInt36) {
    def result = binaryUInt36.toCharArray()
    for (int i in 0..35) {
        if (mask[i] == '0') {
            result[i] = '0'
        } else if (mask[i] == '1') {
            result[i] = '1'
        } else if (mask[i] == 'X') {
            assert true
        } else {
            assert false
        }
    }
    return String.valueOf(result)
}

def memoryWrite(memory, mask, address, value) {
    def val = toBinaryUInt36(value)
    memory[address] = applyMask(mask, val)
}

def sumOfMemory(memory) {
    def sum = 0G
    for (value in memory) {
        if (value) {
            sum += fromBinaryUInt36(value)
        }
    }
    return sum
}

new File("input").eachLine { line ->
    if (line.startsWith("mask")) {
        def matcher = line =~ /^mask = (?<mask>[X01]+)$/
        assert matcher.matches()
        mask = matcher.group("mask")
    } else {
        def matcher = line =~ /^mem\[(?<address>\d+)\] = (?<value>\d+)$/
        assert matcher.matches()
        memoryWrite(memory, mask, Integer.parseInt(matcher.group("address")), new BigInteger(matcher.group("value")))
    }
}

println sumOfMemory(memory)
