this.getClass().classLoader.rootLoader.addURL(new File("combinatoricslib3-3.3.0.jar").toURL())

def mask
def memory = [:]

def toBinaryUInt36(value) {
    return value.toString(2).padLeft(36, "0")
}

def fromBinaryUInt36(binaryUInt36) {
    return new BigInteger(binaryUInt36, 2)
}

def applyAddressMask(mask, address) {
    def result = address.toCharArray()
    for (int i in 0..35) {
        if (mask[i] == '0') {
            assert true
        } else if (mask[i] == '1') {
            result[i] = '1'
        } else if (mask[i] == 'X') {
            result[i] = 'X'
        } else {
            assert false
        }
    }
    return String.valueOf(result)
}

def applyMask(mask, binaryUInt36) {
    def result = binaryUInt36.toCharArray()
    for (int i in 0..35) {
        if (mask[i] == '0') {
            result[i] = '0'
        } else if (mask[i] == '1') {
            result[i] = '1'
        } else if (mask[i] == 'X') {
            assert true
        } else {
            assert false
        }
    }
    return String.valueOf(result)
}

def resolveFloatingAddress(address) {
    def result = []
    def numFloating = address.count("X")
    def Gen = Class.forName("org.paukov.combinatorics3.Generator").newInstance();
    Gen.permutation(0, 1)
        .withRepetitions(numFloating)
        .stream()
        .forEach {
            StringBuffer res = new StringBuffer();
            def matcher = address =~ /[X]/;
            int pos = 0;
            while (matcher.find()) {
                String replacement = pos != it.size() ? it[pos++] : "X";
                matcher.appendReplacement(res, replacement);
            } 
            matcher.appendTail(res);
            result << res.toString()
        }
    return result
}

def memoryWrite(memory, mask, address, value) {
    def val = toBinaryUInt36(value)
    def binaryAddress = toBinaryUInt36(address)
    def floatingAddress = applyAddressMask(mask, binaryAddress)
    def addresses = resolveFloatingAddress(floatingAddress)
    for (a in addresses) {
        memory[fromBinaryUInt36(a)] = val
    }
}

def sumOfMemory(memory) {
    def sum = 0G
    for (entry in memory) {
        if (entry) {
            sum += fromBinaryUInt36(entry.value)
        }
    }
    return sum
}

new File("input").eachLine { line ->
    if (line.startsWith("mask")) {
        def matcher = line =~ /^mask = (?<mask>[X01]+)$/
        assert matcher.matches()
        mask = matcher.group("mask")
    } else {
        def matcher = line =~ /^mem\[(?<address>\d+)\] = (?<value>\d+)$/
        assert matcher.matches()
        memoryWrite(memory, mask, new BigInteger(matcher.group("address")), new BigInteger(matcher.group("value")))
    }
}

println sumOfMemory(memory)
