import scala.collection.mutable.Map
import scala.collection.mutable.ArrayBuffer

var input: ArrayBuffer[Int] = ArrayBuffer(14, 1, 17, 0, 3, 20)
var inputTest: ArrayBuffer[Int] = ArrayBuffer(3, 2, 1)

var game: Map[Int, ArrayBuffer[Int]] = Map()
var history: ArrayBuffer[Int] = ArrayBuffer()

def speak(turn: Int, number: Int) = {
    if (!game.contains(number)) {
        game(number) = ArrayBuffer(turn)
    } else {
        game(number) += turn
    }
    history.insert(turn, number)
}

var seed = inputTest // select input here
seed.zipWithIndex.foreach({ case (number, index) => {
    speak(index, number)
}})

var turn = seed.length
while (turn < 30000000) {
    var last = history.last
    if (game.contains(last)) {
        if (game(last).length < 2) {
            // number was only spoken once
            speak(turn, 0)
        } else {
            // number was spoken at least two times before
            speak(turn, game(last)(game(last).length - 1) - game(last)(game(last).length - 2))
        }
    } else {
        assert(false)
    }
    turn += 1
}

println(s"The number spoken in turn 2020 is ${history.last}.")
