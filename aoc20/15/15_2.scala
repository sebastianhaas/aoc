import scala.collection.mutable.HashMap
import java.time.LocalDateTime

/**
 * Must be executed with increased heap space:
 * scala -J-Xmx2g 15_2.scala
 */

var input: Array[Int] = Array(14, 1, 17, 0, 3, 20)
var inputTest: Array[Int] = Array(3, 1, 2)

var game: HashMap[Int, Array[Int]] = HashMap()
var last: Int = 0

var seed = input // <- select from above input here
seed.zipWithIndex.foreach({ case (number, index) => {
    game(number) = Array(-1, index)
    last = number
}})

// Ensure 0 exists
if (!game.contains(0)) {
    game(0) = Array(-1, -1)
}

var turn = seed.length
while (turn < 30000000) {
    var lastHistory = game(last)
    if (lastHistory(0) == -1) {
        // number was only spoken once
        var gameNumber = game(0)
        gameNumber(0) = gameNumber(1)
        gameNumber(1) = turn
        last = 0
    } else {
        // number was spoken at least two times before
        var speak = lastHistory(1) - lastHistory(0)
        if (game.contains(speak)) {
            var gameNumber = game(speak)
            gameNumber(0) = gameNumber(1)
            gameNumber(1) = turn
        } else {
            game(speak) = Array(-1, turn)
        }
        last = speak
    }

    turn += 1
    if (turn % 1000000 == 0) {
        println(f"${LocalDateTime.now()} - ${turn / 300000}%.0f%%")
    }
}

println(s"\nThe number spoken in turn 30000000 is ${last}.")
