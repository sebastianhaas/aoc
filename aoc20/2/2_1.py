import re

with open('input', 'r') as file:
	db = file.read()

regex = r"(?P<min>\d+)-(?P<max>\d+) (?P<character>\D): (?P<password>\D+)\n"
compliant = 0
for m in re.finditer(regex, db):
	if int(m.group('min')) <= m.group('password').count(m.group('character')) <= int(m.group('max')):
		compliant += 1

print(f"{compliant} passwords are compliant")
