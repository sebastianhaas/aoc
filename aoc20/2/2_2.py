import re

with open('input', 'r') as file:
	db = file.read()

regex = r"(?P<one>\d+)-(?P<two>\d+) (?P<character>\D): (?P<password>\D+)\n"
compliant = 0
for m in re.finditer(regex, db):
	one = m.group('password')[int(m.group('one')) - 1] == m.group('character')
	two = m.group('password')[int(m.group('two')) - 1] == m.group('character')
	if (one and not two) or (two and not one):
		compliant += 1

print(f"{compliant} passwords are compliant")
