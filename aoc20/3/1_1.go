package main

import (
    "bufio"
    "log"
    "os"
)

func main() {
    treemap := read_input()
    print(find_trees_on_path(treemap))
}

func find_trees_on_path(treemap [][]rune) int {
    t_encountered, x, y := 0, 0, 0

    for y < (len(treemap) - 1) {
        x += 3
        y += 1
        if treemap[y][x % len(treemap[y])] == '#' {
            t_encountered++
        }
    }

    return t_encountered
}

func read_input() [][]rune {
    var treemap [][]rune

    file, err := os.Open("input")
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := []rune{}
        for _, character := range scanner.Text() {
            line = append(line, character)
        }
        treemap = append(treemap, line)
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
    return treemap
}
