package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
)

type slope struct {
    x int
    y  int
}

func main() {
    treemap := read_input()
    
    slopes := [5]slope {
        slope { x: 1, y: 1 },
        slope { x: 3, y: 1 },
        slope { x: 5, y: 1 },
        slope { x: 7, y: 1 },
        slope { x: 1, y: 2 },
    }

    trees_on_paths := 1
    for _, slope := range slopes { 
        trees_on_paths *= find_trees_on_path(treemap, slope)
    }
    fmt.Println("Total trees on all paths: ", trees_on_paths)
}

func find_trees_on_path(treemap [][]rune, a slope) int {
    t_encountered, x, y := 0, 0, 0

    for y < (len(treemap) - 1) {
        x += a.x
        y += a.y
        if treemap[y][x % len(treemap[y])] == '#' {
            t_encountered++
        }
    }

    return t_encountered
}

func read_input() [][]rune {
    var treemap [][]rune

    file, err := os.Open("input")
    if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := []rune{}
        for _, character := range scanner.Text() {
            line = append(line, character)
        }
        treemap = append(treemap, line)
    }

    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }
    return treemap
}
