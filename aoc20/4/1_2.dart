import 'dart:convert';
import 'dart:io';

void main() {
    processsInput();
}

Future processsInput() async {
    var lineNumber = 1;
    final lines = utf8.decoder
        .bind(File('input').openRead())
        .transform(const LineSplitter());
    try {
        var validCount = 0;
        var totalCount = 0;
        String data = "";
        await for (var line in lines) {
            if (line.trim().isEmpty) {
                var passport = Passport.parse(data);
                if (passport.isValid()) {
                    validCount++;
                }
                data = "";
                totalCount++;
            }
            data += " " + line;
        }
        stdout.writeln("${validCount} of ${totalCount} passports are valid.");
    } catch (err) {
        stdout.writeln(err);
        exitCode = 24122020;
    }
}

class Passport {
    String birthYear;
    String issueYear;
    String expirationYear;
    String height;
    String hairColour;
    String eyeColour;
    String passportId;
    String countryId;

    bool isValid() {
        return _isValidBirthYear() &&
            _isValidIssueYear() &&
            _isValidExpirationYear() &&
            _isValidHeight() &&
            _isValidHairColour() &&
            _isValidEyeColour() &&
            _isValidPassportId() &&
            _isValidCountryId();
    }

    bool _isValidBirthYear() {
        return _isInIntegerRange(birthYear, 1920, 2020);
    }

    bool _isValidIssueYear() {
        return _isInIntegerRange(issueYear, 2010, 2020);
    }

    bool _isValidExpirationYear() {
        return _isInIntegerRange(expirationYear, 2020, 2030);
    }

    bool _isValidHairColour() {
        return _matchesRegExp(hairColour, new RegExp(r'^#[0-9a-f]{6}$'));
    }

    bool _isValidEyeColour() {
        return _matchesRegExp(eyeColour, new RegExp(r'^(amb|blu|brn|gry|grn|hzl|oth)$'));
    }

    bool _isValidPassportId() {
        return _matchesRegExp(passportId, new RegExp(r'^\d{9}$'));
    }

    bool _isValidCountryId() {
        return true;
    }

    bool _isValidHeight() {
        if (!_isNullOrWhitespace(height)) {
            RegExp re = new RegExp(r'^(\d+)(cm|in)$');
            var match = re.firstMatch(height);
            if (match != null) {
                if (match.group(2) == "cm") {
                    return _isInIntegerRange(match.group(1), 150, 193);
                } else if (match.group(2) == "in") {
                    return _isInIntegerRange(match.group(1), 59, 76);
                }
            }
        }
        return false;
    }

    bool _isInIntegerRange(String input, int min, int max) {
        if (!_isNullOrWhitespace(input)) {
            var n = int.tryParse(input);
            if (n != null) {
                return min <= n && n <= max;
            }
        }
        return false;
    }

    bool _isNullOrWhitespace(String input) {
        return input == null || input.trim().isEmpty;
    }

    bool _matchesRegExp(input, RegExp re) {
        if (!_isNullOrWhitespace(input)) {
            return re.hasMatch(input);
        }
        return false;
    }

    static Passport parse(String data) {
        var passport = Passport();
        var tokens = data.split(' ');
        for (final token in tokens) {
            var pair = token.split(':');
            switch (pair[0]) {
                case 'byr':
                    passport.birthYear = pair[1];
                    break;
                case 'iyr':
                    passport.issueYear = pair[1];
                    break;
                case 'eyr':
                    passport.expirationYear = pair[1];
                    break;
                case 'hgt':
                    passport.height = pair[1];
                    break;
                case 'hcl':
                    passport.hairColour = pair[1];
                    break;
                case 'ecl':
                    passport.eyeColour = pair[1];
                    break;
                case 'pid':
                    passport.passportId = pair[1];
                    break;
                case 'cid':
                    passport.countryId = pair[1];
                    break;
            }
        }
        return passport;
    }
}
