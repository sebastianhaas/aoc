#include <stdio.h>
#include <string.h>

struct Seat {
   int row;
   int column;
};

#define MAX_LEN 24

struct Seat decode_seat(char *seat) {
    int row, column;
    int i;
    int len = strlen(seat);
    int row_lower = 0;
    int row_upper = 127;
    int column_lower = 0;
    int column_upper = 7;
    for (i = 0; i < len; i++) {
        if (i < 7) {
            if (seat[i] == 'F') {
                row_upper = row_upper - 1 - (row_upper - row_lower) / 2;
            } else if (seat[i] == 'B') {
                row_lower = row_lower + 1 + (row_upper - row_lower) / 2;
            }
            row = row_lower;
        } else {
            if (seat[i] == 'L') {
                column_upper = column_upper - 1 - (column_upper - column_lower) / 2;
            } else if (seat[i] == 'R') {
                column_lower = column_lower + 1 + (column_upper - column_lower) / 2;
            }
            column = column_lower;
        }
    }
    struct Seat decoded_seat = { .row = row, .column = column };
    return decoded_seat;
}

int calculate_seat_id(struct Seat *seat) {
    return seat->row * 8 + seat->column;
}

int read_input() {
    int highest_seat_id = -1;
    FILE* fp;
    fp = fopen("input", "r");
    if (fp == NULL) {
      perror("Failed: ");
    }

    char buffer[MAX_LEN];
    while (fgets(buffer, MAX_LEN - 1, fp))
    {
        buffer[strcspn(buffer, "\n")] = 0;
        struct Seat seat = decode_seat(buffer);
        int seat_id = calculate_seat_id(&seat);
        if (seat_id > highest_seat_id) {
            highest_seat_id = seat_id;
        }
    }

    fclose(fp);
    return highest_seat_id;
}

int main() {
    int highest_seat_id = read_input();
    printf("The highest seat ID on a boarding pass is: %d\n", highest_seat_id);
}
