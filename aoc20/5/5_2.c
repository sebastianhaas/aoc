#include <stdio.h>
#include <string.h>

struct Seat {
   int row;
   int column;
};

#define MAX_LEN 13
#define MAX_SEAT_IDS 932

struct Seat decode_seat(char *seat) {
    int row, column;
    int i;
    int len = strlen(seat);
    int row_lower = 0;
    int row_upper = 127;
    int column_lower = 0;
    int column_upper = 7;
    for (i = 0; i < len; i++) {
        if (i < 7) {
            if (seat[i] == 'F') {
                row_upper = row_upper - 1 - (row_upper - row_lower) / 2;
            } else if (seat[i] == 'B') {
                row_lower = row_lower + 1 + (row_upper - row_lower) / 2;
            }
            row = row_lower;
        } else {
            if (seat[i] == 'L') {
                column_upper = column_upper - 1 - (column_upper - column_lower) / 2;
            } else if (seat[i] == 'R') {
                column_lower = column_lower + 1 + (column_upper - column_lower) / 2;
            }
            column = column_lower;
        }
    }
    struct Seat decoded_seat = { .row = row, .column = column };
    return decoded_seat;
}

int calculate_seat_id(struct Seat *seat) {
    return seat->row * 8 + seat->column;
}

void read_input(int seat_ids[MAX_SEAT_IDS]) {
    FILE* fp;
    fp = fopen("input", "r");
    if (fp == NULL) {
      perror("Failed: ");
    }

    char buffer[MAX_LEN];
    int i = 0;
    while (fgets(buffer, MAX_LEN - 1, fp))
    {
        buffer[strcspn(buffer, "\n")] = 0;
        struct Seat seat = decode_seat(buffer);
        int seat_id = calculate_seat_id(&seat);
        if (i < MAX_SEAT_IDS) {
            seat_ids[i] = seat_id;
        } else {
            perror("exceeded defined max seat ids");
        }
        i++;
    }
    fclose(fp);
}

void swap(int *xp, int *yp) { 
    int temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
}

void bubblesort(int arr[], int n) { 
   int i, j; 
   for (i = 0; i < n-1; i++) {
       for (j = 0; j < n-i-1; j++) {
           if (arr[j] > arr[j+1]) {
              swap(&arr[j], &arr[j+1]);
           }
       }
   }
}

int main() {
    int i = 0;
    int seat_ids[MAX_SEAT_IDS];
    read_input(seat_ids);
    bubblesort(seat_ids, MAX_SEAT_IDS);
    for (i = 0; i < MAX_SEAT_IDS; i++) {
        if (i > 0 && i < MAX_SEAT_IDS - 2) {
            if (seat_ids[i + 1] != seat_ids[i] + 1) {
                printf("The missing seat ID is %d\n", seat_ids[i] + 1);
            }
        } 
    }
}
