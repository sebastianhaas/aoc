use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("input_test")?;
    let reader = BufReader::new(file);

    let mut groups: Vec<Vec<char>> = Vec::new();
    let mut answers: Vec<char> = Vec::new();
    for line in reader.lines() {
        match line {
            Ok(line) => {
                if line == "" {
                    groups.push(answers.to_owned());
                    answers.clear();
                } else {
                    for c in line.chars() { 
                        if !answers.contains(&c) {
                            answers.push(c);
                        }
                    }
                }
            },
            Err(e) => println!("error parsing input file: {:?}", e),
        }
    }
    groups.push(answers.to_owned());

    println!("The total sum of answers in all groups is: {0}", get_answers_of_all_groups(&groups));

    Ok(())
}

fn get_answers_of_group(answers: &Vec<char>) -> usize {
    return answers.len();
}

fn get_answers_of_all_groups(groups: &Vec<Vec<char>>) -> usize {
    let mut total = 0;
    for answers in groups.iter() {
        total += get_answers_of_group(answers);
    }
    return total;
}
