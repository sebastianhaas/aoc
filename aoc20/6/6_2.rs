use std::fs::File;
use std::io::{self, prelude::*, BufReader};

fn main() -> io::Result<()> {
    let file = File::open("input")?;
    let reader = BufReader::new(file);

    let mut groups: Vec<Vec<Vec<char>>> = Vec::new();
    let mut group: Vec<Vec<char>> = Vec::new();
    for line in reader.lines() {
        match line {
            Ok(line) => {
                if line == "" {
                    groups.push(group.to_owned());
                    group.clear();
                } else {
                    let mut answers: Vec<char> = Vec::new();
                    for c in line.chars() {
                        answers.push(c);
                    }
                    group.push(answers.to_owned());
                }
            },
            Err(e) => println!("error parsing input file: {:?}", e),
        }
    }
    groups.push(group.to_owned());

    println!("The total sum of answers in all groups is: {0}", get_answers_of_all_groups(&groups));

    Ok(())
}

fn get_answers_of_group(group: &Vec<Vec<char>>) -> usize {
    let mut all_yes: Vec<char> = Vec::new();
    let mut tmp: Vec<char>;
    let mut first = true;
    for person in group.iter() {
        if first {
            for c in person.iter() {
                all_yes.push(*c);
            }
            first = false;
        } else {
            tmp = Vec::new();
            for a in all_yes.iter() {
                if person.contains(&a) {
                    tmp.push(*a);
                }
            }
            all_yes = tmp.to_owned();
            tmp.clear();
        }
    }
    return all_yes.len();
}

fn get_answers_of_all_groups(groups: &Vec<Vec<Vec<char>>>) -> usize {
    let mut total = 0;
    for group in groups.iter() {
        total += get_answers_of_group(group);
    }
    return total;
}
