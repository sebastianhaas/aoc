library(graph)
library(Rgraphviz)
library(stringr)

# Uncomment this line for a PDF output of the resulting graph.
# This is only feasible for the test input, the real input is much too complex.
# pdf(file="~/Documents/R.pdf")

processFile = function(filepath, graph) {
  con = file(filepath, "r")
  while ( TRUE ) {
    line = readLines(con, n = 1)
    if ( length(line) == 0 ) {
      break
    }
    match = str_match(line, "^(\\D*?) bags contain (.*)$")
    color = match[1,2]
    rest = match[1,3]

    if (all(rest != "no other bags.")) {
    	innerBags = str_split(rest, ", ", simplify = TRUE)
    	for (inner in innerBags) {
			match = str_match(inner, "^(\\d) (\\D+) (bags[\\.]{0,1}|bag[\\.]{0,1})$")
			if (!is.element(color, nodes(graph))) {
				graph <- addNode(color, graph)
			}
			if (!is.element(match[1,3], nodes(graph))) {
				graph <- addNode(match[1,3], graph)
			}
			graph <- addEdge(color, match[1,3], graph, as.numeric(match[1,2]))
		}
    }

  }
  close(con)
  return(graph)
}

get_contained_bags = function(node, graph) {
	children = edges(graph, node) # returns adjacent nodes (contained bags)
	contained_bags = 1 # account for the present node
	for (child in children[[1]]) {
		count = edgeData(graph, node, child, "weight")[[1]]
		contained_bags <- contained_bags + count * get_contained_bags(child, graph)
	}
	return(contained_bags)
}

# Create a graph
g1 = graphNEL(edgemode="directed")
g1 <- processFile("input", g1)

contained_bags <- get_contained_bags("shiny gold", g1) - 1 # We don't count our start node (bag)
print(contained_bags)

# Uncomment this line for a PDF output of the resulting graph.
# This is only feasible for the test input, the real input is much too complex.
# ew <- as.character(unlist(edgeWeights(g1)))
# ew <- ew[setdiff(seq(along=ew), removedEdges(g1))]
# names(ew) <- edgeNames(g1)
# eAttrs <- list()
# attrs <- list()
# eAttrs$label <- ew
# attrs$edge$fontsize <- 27
# plot(g1, nodeAttrs=makeNodeAttrs(g1, shape="circle", fillcolor="orange"), edgeAttrs=eAttrs, attrs=attrs)
# dev.off()
