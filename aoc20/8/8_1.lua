program = {}
local inputFile = io.open("input", "r")
local i = 0
for line in inputFile:lines() do
	program[i] = line
	i = i + 1
end

run = true
accumulator = 0
instruction_pointer = 0
instruction_history = {}
while (run) do

	if instruction_history[instruction_pointer] then
		print("INFINITE LOOP")
		run = false
		break
	else
		instruction_history[instruction_pointer] = true
	end

	line = program[instruction_pointer]
	cmd, sign, number = line:match("^(%a+) (%p)(%d+)$")
	
	if (cmd == "acc") then
		if (sign == "+") then
			accumulator = accumulator + number
		else
			accumulator = accumulator - number
		end
		instruction_pointer = instruction_pointer + 1
	
	elseif (cmd == "nop") then
		instruction_pointer = instruction_pointer + 1
	
	elseif (cmd == "jmp") then
		if (sign == "+") then
			instruction_pointer = instruction_pointer + number
		else
			instruction_pointer = instruction_pointer - number
		end
	end

end

print("Accumulator: ", accumulator)
