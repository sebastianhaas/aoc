program = {}
local inputFile = io.open("input", "r")
local i = 0
for line in inputFile:lines() do
	program[i] = line
	i = i + 1
end
local program_length = i - 1

function execute(program, program_length)
	run = true
	result_code = 0
	accumulator = 0
	instruction_pointer = 0
	instruction_history = {}
	while (run) do

		if (instruction_pointer == program_length) then
			print("REGULAR PROGRAM END")
			run = false
			result_code = 0
			break
		end

		if instruction_history[instruction_pointer] then
			print("INFINITE LOOP")
			run = false
			result_code = -1
			break
		else
			instruction_history[instruction_pointer] = true
		end

		line = program[instruction_pointer]
		cmd, sign, number = line:match("^(%a+) (%p)(%d+)$")
		
		if (cmd == "acc") then
			if (sign == "+") then
				accumulator = accumulator + number
			else
				accumulator = accumulator - number
			end
			instruction_pointer = instruction_pointer + 1
		
		elseif (cmd == "nop") then
			instruction_pointer = instruction_pointer + 1
		
		elseif (cmd == "jmp") then
			if (sign == "+") then
				instruction_pointer = instruction_pointer + number
			else
				instruction_pointer = instruction_pointer - number
			end
		end

	end

	print("Accumulator: ", accumulator)
	return result_code
end

function try_fix_program(program, program_length)
	for idx, line in pairs(program) do
		tmp = line
		if string.find(line, "nop") then
			line = string.gsub(line, "nop", "jmp")
		elseif string.find(line, "jmp") then
			line = string.gsub(line, "jmp", "nop")
		end
		program[idx] = line
		result_code = execute(program, program_length)
		if (result_code >= 0) then
			break
		end
		program[idx] = tmp
 	end
end

try_fix_program(program, program_length)
