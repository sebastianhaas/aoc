input_filename = "input"
preamble_length = 25

defmodule AOC do
  def is_sum_of_preamble(preamble, x) do
    Enum.find(preamble, fn a ->
        Enum.find(preamble, fn b ->
            if a != b do
                x == elem(a, 0) + elem(b, 0)
            else
                false
            end
        end)
    end)
  end

  def find_first_invalid_number(data, preamble_length) do
    Enum.find(data, fn x ->
        if elem(x, 1) >= preamble_length do
            preamble = Enum.slice(data, elem(x, 1) - preamble_length, preamble_length)
            !is_sum_of_preamble(preamble, elem(x, 0))
        end
    end)
  end
end

result = File.stream!(input_filename)
|> Stream.map(&String.trim(&1))
|> Stream.map(&String.to_integer(&1))
|> Stream.with_index
|> Enum.to_list
|> AOC.find_first_invalid_number(preamble_length)

IO.puts("First value not to be the sum of two in the preamble: #{elem(result, 0)}")
