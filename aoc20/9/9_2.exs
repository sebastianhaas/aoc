input_filename = "input"
preamble_length = 25

defmodule AOC do

  def is_sum_of_preamble(preamble, x) do
    Enum.find(preamble, fn a ->
        Enum.find(preamble, fn b ->
            if a != b do
                x == elem(a, 0) + elem(b, 0)
            else
                false
            end
        end)
    end)
  end

  def find_first_invalid_number(data, preamble_length) do
    Enum.find(data, fn x ->
        if elem(x, 1) >= preamble_length do
            preamble = Enum.slice(data, elem(x, 1) - preamble_length, preamble_length)
            !is_sum_of_preamble(preamble, elem(x, 0))
        end
    end)
  end

  def find_first_contiguous_set(data, target) do
    Enum.find_value(data, fn a ->
      current_idx = elem(a, 1)
      result = data
      |> Enum.drop(current_idx)
      |> Enum.reduce_while({[], 0}, fn x, acc ->
        cond do
          elem(acc, 1) == target ->
            {:halt, acc}
          elem(acc, 1) > target ->
            {:halt, {[], -1}}
          elem(acc, 1) < target ->
            {:cont, {elem(acc, 0) ++ [x] , elem(acc, 1) + elem(x, 0)}}
        end
      end)
      
      if elem(result, 1) == target do
        Enum.map(elem(result, 0), fn x -> elem(x, 0) end)
      end
    end)
  end

end

input = File.stream!(input_filename)
|> Stream.map(&String.trim(&1))
|> Stream.map(&String.to_integer(&1))
|> Stream.with_index
|> Enum.to_list

# Find first invalid number
invalid_number = input
|> AOC.find_first_invalid_number(preamble_length)
|> elem(0)
IO.puts("Found first invalid number: #{invalid_number}")

# Find first contiguos set that adds up to the invalid number
result = input
|> AOC.find_first_contiguous_set(invalid_number)

lowest = Enum.min(result)
highest = Enum.max(result)
IO.puts("Found first contiguous set. Lowest: #{lowest}, highest: #{highest}, sum: #{lowest + highest}")
