file1 = open('input', 'r')
lines = file1.readlines()

elves = []
cals = 0

for line in lines:
  if line.isspace():
    elves.append(cals)
    cals = 0
  else:
    cals += int(line)

print(max(elves))

