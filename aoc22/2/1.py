from enum import Enum, auto


class MalformedInputException(Exception):

  def __init__(self, line_number: int, *args: object) -> None:
    super().__init__(*args)
    self.line_number = line_number

  def __str__(self) -> str:
    return f"{super().__str__()} at line {self.line_number}"


class InvalidSymbolException(MalformedInputException):
  def __init__(self, symbol: str, line_number: int, *args: object) -> None:
    super().__init__(line_number, *args)
    self.symbol = symbol

  def __str__(self) -> str:
    return f"invalid symbol '{self.symbol}' at line {self.line_number}"


class Shape(str, Enum):
  ROCK = auto()
  PAPER = auto()
  SCISSORS = auto()


class Result(Enum):
  LOSS = auto()
  DRAW = auto()
  WIN = auto()


class Round():
  def __init__(self, opponent: Shape, you: Shape) -> None:
    if opponent == None or you == None:
      raise Exception("round must have both the opponent's and your shape")
    self.opponent = opponent
    self.you = you

  result_map = {
    # Map round results
    # (opponent, you): result
    (Shape.PAPER, Shape.PAPER): Result.DRAW,
    (Shape.PAPER, Shape.ROCK): Result.LOSS,
    (Shape.PAPER, Shape.SCISSORS): Result.WIN,
    (Shape.ROCK, Shape.PAPER): Result.WIN,
    (Shape.ROCK, Shape.ROCK): Result.DRAW,
    (Shape.ROCK, Shape.SCISSORS): Result.LOSS,
    (Shape.SCISSORS, Shape.PAPER): Result.LOSS,
    (Shape.SCISSORS, Shape.ROCK): Result.WIN,
    (Shape.SCISSORS, Shape.SCISSORS): Result.DRAW,
  }

  def get_result(self) -> Result:
    return self.result_map[(self.opponent, self.you)]


class RockPaperScissorsGame():

  result_score_map = {
    Result.LOSS: 0,
    Result.DRAW: 3,
    Result.WIN: 6
  }

  shape_score_map = {
    Shape.ROCK: 1,
    Shape.PAPER: 2,
    Shape.SCISSORS: 3
  }

  def __init__(self, rounds: list[Round]) -> None:
    self.rounds = rounds

  def get_your_total_score(self) -> int:
    score = 0

    for round in self.rounds:
      result = round.get_result()
      score += RockPaperScissorsGame.result_score_map[result]
      score += RockPaperScissorsGame.shape_score_map[round.you]

    return score


class InputParser():

  opponent_shape_map = {
    "A": Shape.ROCK,
    "B": Shape.PAPER,
    "C": Shape.SCISSORS,
  }

  you_shape_map = {
    "X": Shape.ROCK,
    "Y": Shape.PAPER,
    "Z": Shape.SCISSORS,
  }

  @staticmethod
  def parse_input(input_file: str) -> list[Round]:
    result = []

    line_number = 0
    input = open(input_file, 'r')
    for line in input:
      tokens = line.strip().split(' ')

      if not len(tokens) == 2:
        raise MalformedInputException(line, "expecting exactly two tokens per line")
      if not tokens[0] in InputParser.opponent_shape_map:
        raise InvalidSymbolException(tokens[0], line_number, "invalid opponent shape")
      if not tokens[1] in InputParser.you_shape_map:
        raise InvalidSymbolException(tokens[1], line_number, "invalid you shape")

      result.append(Round(
        InputParser.opponent_shape_map[tokens[0]],
        InputParser.you_shape_map[tokens[1]]
      ))
      line_number += 1
    
    return result


# Solve the puzzle
try:
  rounds = InputParser.parse_input("input")
  game = RockPaperScissorsGame(rounds)
  total_score = game.get_your_total_score()

  print("Your total score is:", total_score)

except Exception as ex:
  print(f"failed to parse input: {ex}")
